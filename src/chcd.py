#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" 漢字の文字コードを出力するサンプルプログラム。 """

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2022, @HirMtsd"
__date__      = "2022/12/09"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1"
__status__    = "Development"

# 入力ファイルの文字コードはutf-8。
# 暫定的に入力ファイル名をsample_utf8.txtに固定している。

# Windows環境で、venvを使用している場合、shebangのせいで、
# 「ModuleNotFoundError: No module named 'cv2'」
# が表示される場合は、pyランチャを使用しないで「python qr_r.py」と起動する。
# https://www.python.org/dev/peps/pep-0397/

# readlinesで一括読み込み
with open('sample_utf8.txt', "r", encoding="utf-8") as f:
  lines = f.readlines()

for line in lines:
  trg = line[0:1]

  print(trg)
  print("U+" + format(ord(trg), 'x'))

  print("(SJIS)" + (trg.encode('Shift-JIS')).hex())
  print(" (JIS)" + ((trg.encode('ISO-2022-JP')).hex().replace('1b2442','').replace('1b2842','')))
